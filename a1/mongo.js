//Courses Collection - MongoDB CRUD Operations

use myFirstDB

db.courses.insertMany([
		{name: "Basic HTML", 
		price: 450, 
		isActive: true, 
		description: "Introduction to Basic HTML 5",
		enrollees: []
		},
		{name: "Basic CSS", 
		price: 600, 
		isActive: true, 
		description: "Introduction to Basic CSS 3",
		enrollees: []
		},
		{
		name: "Basic JavaScript",
		price: 750, 
		isActive: true, 
		description: "Introduction to Basic JavaScript",
		enrollees: [
			{
				userID: ObjectId("61fbcab1fb5fca1da88b0b2e")
			}
		]
		}
		
	])