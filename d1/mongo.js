/*
	Using MongoDB Locally

	To use MongoDB on your local machine, you must first open a terminal and type the command "mongod" to run the MongoDB Community Server.

	The server must always be running for the duration of your use of your local MongoDB interface/ Mongo Shell.

	Open a separate terminal and type "mongo" to open the Mongo Shell which is a command line interface that allows us to issue MongoDB commands. 
*/

// MongoDB Commands:

//show list of databases

show dbs

// use <database name>

use myFirstDB

//the use cimand is only the first step in creating a database. Next, you must create a single record for the database to actually be created.

// create a single document

db.users.insertOne({name: "John Smith", age: 20, isActive: true})

//Get a list of all users

db.users.find()

//create a second user

db.users.insertOne({name: "Jane Doe", age: 21, isActive: false})

//Within each database is a number of collections, where multiple related data is stored (users, pasts, products, etc)

//Create a collection - If a collection does not exist yet, MongoDB creates the collection when you first store data for that collection

db.products.insertMany([
		{name: "Product1", price: 200.50, stock: 100, description: "Lorem Ipsum"},
		{name: "Product2", price: 333, stock: 25, description: "Lorem Ipsum"}
	])

db.products.find() // - Get a list of all products

//Find a specific document (product):

db.products.find({name: "Product2"})

// Update an existing document:

db.users.updateOne(
		{_id: ObjectId("61fbcab1fb5fca1da88b0b2e")}, // WHAT to Update
		{
			$set: {isActive: true} //HOW to update it
		}
	)

// Update to add a new field:

db.users.updateOne(
		{_id: ObjectId("61fbcab1fb5fca1da88b0b2e")},
		{
			$set: {
				address: {
					street: "123 Street st.",
					city: "New York",
					country: "United States"
				}
			}
		}
	)

// Format results in a more presentable way

db.users.find().pretty()

//Delete a document - (specific)

db.users.deleteOne({_id: ObjectId("61fbc9cdfb5fca1da88b0b2d")})